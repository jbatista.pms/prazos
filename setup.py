from setuptools import setup

setup(
    name='prazos',
    version='0.3',
    url='https://github.com/batistasan/prazos',
    license='MIT License',
    author='João Batista dos Santos Filho',
    author_email='joaobatista.sf@hotmail.com',
    keywords='prazos time tempo',
    description=u'Cálculo de prazos e datas de vencimento.',
    packages=['prazos'],
    install_requires=[],
)
