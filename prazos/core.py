from calendar import monthrange
from datetime import date, timedelta


class Inicio(object):
    def __init__(self, data_base):
        self.__data_base = data_base

    def _acrescentar(self, data_inicial, acrescentar, dia_base):
        # Ano e mês futuro
        anos_ult, mes_futuro = self._acrescentar_mes(
            data_inicial.month, acrescentar
            )
        ano_futuro = data_inicial.year + anos_ult

        # Dia futuro
        ultimo_dia_mes = monthrange(ano_futuro, mes_futuro)[1]
        if ultimo_dia_mes >= dia_base:
            dia_futuro = dia_base
        else:
            dia_futuro = 1
            anos_ult, mes_futuro = self._acrescentar_mes(mes_futuro, 1)
            ano_futuro += anos_ult
        return date(ano_futuro, mes_futuro, dia_futuro)

    def _acrescentar_mes(self, mes, acrescentar):
        # Ano e mês futuro
        mes_futuro = mes + acrescentar
        anos_ultrapassados = (mes_futuro-1) // 12
        if anos_ultrapassados:
            mes_futuro = mes_futuro - anos_ultrapassados * 12
        return anos_ultrapassados, mes_futuro

    def _subtr(self, data, subtr):
        # Caso os dias não possam coincidir entre as datas do prazo.
        return data - timedelta(days=1) if subtr else data

    def prazo(self, intervalo, dia_base=None, coincidir=True):
        """
        Retorna o prazo no intervalo pretendido
        Caso deseje, os dias das datas não concidirão, desde que o parametro
        coincidir seja False.
        """
        dia_base = dia_base or self.__data_base.day
        resultado = self._acrescentar(self.__data_base, intervalo, dia_base)
        return self._subtr(data=resultado, subtr=not coincidir)

    def prazos(self, quantidade, intervalo, coincidir=True):
        """
        Retorna uma tupla com tuplas contendo a data inicial e final na
        quantidade de prazos pretendidos.
        Caso deseje, os dias das datas não concidirão, desde que o parametro
        coincidir seja False.
        """
        prazos = []
        ultimo = self.__data_base
        dia_Base = self.__data_base.day
        data_inicial = self.__data_base
        for i in range(quantidade):
            ultimo = resultado = self._acrescentar(ultimo, intervalo, dia_Base)
            # Caso os dias não possam coincidir entre as datas do prazo.
            data_final = self._subtr(data=resultado, subtr=not coincidir)
            prazos.append((data_inicial, data_final))
            # Aqui o resultado é guardado como data inicial para o próximo
            # prazo.
            data_inicial = resultado
            # Se o dia_base e o dia no resultado forem diferentes, isso quer
            # dizer que houve um acrescimo de um dia, por dia_base ser maior que
            # o último dia do mês do período encontrado, fazendo com que a data
            # do resultado seja o primeiro dia último do mês seguinte.
            # O decréscimo se faz necessário para não 'confundir' o algorítimo
            # de acréscimo, levando-o a resultados erroneos.
            if ultimo.day != dia_Base:
                ultimo -= timedelta(days=1)
        return tuple(prazos)
