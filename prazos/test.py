import unittest
from datetime import date
from .core import Inicio

class Testes(unittest.TestCase):
    def test_simples(self):
        iara = Inicio(date(2016,1,1))
        prazos = iara.prazos(3,1)
        self.assertEqual(prazos[0], (date(2016,1,1),date(2016,2,1)))
        self.assertEqual(prazos[1], (date(2016,2,1),date(2016,3,1)))
        self.assertEqual(prazos[2], (date(2016,3,1),date(2016,4,1)))

    def test_simples_prazo(self):
        iara = Inicio(date(2016,1,31))
        self.assertEqual(iara.prazo(1),date(2016,3,1))
        self.assertEqual(iara.prazo(2),date(2016,3,31))
        self.assertEqual(iara.prazo(3),date(2016,5,1))
        iara = Inicio(date(2016,1,14))
        self.assertEqual(iara.prazo(1),date(2016,2,14))
        self.assertEqual(iara.prazo(2),date(2016,3,14))
        self.assertEqual(iara.prazo(3),date(2016,4,14))

    def test_simples_prazo_nao_coincidir(self):
        iara = Inicio(date(2016,1,31))
        self.assertEqual(iara.prazo(1, coincidir=False),date(2016,2,29))
        self.assertEqual(iara.prazo(2, coincidir=False),date(2016,3,30))
        self.assertEqual(iara.prazo(3, coincidir=False),date(2016,4,30))
        iara = Inicio(date(2016,1,14))
        self.assertEqual(iara.prazo(1, coincidir=False),date(2016,2,13))
        self.assertEqual(iara.prazo(2, coincidir=False),date(2016,3,13))
        self.assertEqual(iara.prazo(3, coincidir=False),date(2016,4,13))

    def test_simples_nao_coincidir(self):
        iara = Inicio(date(2016,1,1))
        prazos = iara.prazos(4,1,False)
        self.assertEqual(prazos[0], (date(2016,1,1),date(2016,1,31)))
        self.assertEqual(prazos[1], (date(2016,2,1),date(2016,2,29)))
        self.assertEqual(prazos[2], (date(2016,3,1),date(2016,3,31)))
        self.assertEqual(prazos[3], (date(2016,4,1),date(2016,4,30)))
        iara = Inicio(date(2016,1,15))
        prazos = iara.prazos(4,1,False)
        self.assertEqual(prazos[0], (date(2016,1,15),date(2016,2,14)))
        self.assertEqual(prazos[1], (date(2016,2,15),date(2016,3,14)))
        self.assertEqual(prazos[2], (date(2016,3,15),date(2016,4,14)))
        self.assertEqual(prazos[3], (date(2016,4,15),date(2016,5,14)))

    def test_acrecentar(self):
        iara = Inicio(date(2011,1,1))
        self.assertEqual(iara._acrescentar(date(2011,1,1),1,1), date(2011,2,1))
        self.assertEqual(iara._acrescentar(date(2011,1,1),2,1), date(2011,3,1))
        self.assertEqual(iara._acrescentar(date(2011,1,1),3,1), date(2011,4,1))
        self.assertEqual(iara._acrescentar(date(2011,1,31),1,31), date(2011,3,1))
        self.assertEqual(iara._acrescentar(date(2011,1,31),2,31), date(2011,3,31))
        self.assertEqual(iara._acrescentar(date(2011,1,31),3,31), date(2011,5,1))
        self.assertEqual(iara._acrescentar(date(2015,3,1),12,1), date(2016,3,1))
        self.assertEqual(iara._acrescentar(date(2015,1,14),1,14), date(2015,2,14))
        self.assertEqual(iara._acrescentar(date(2015,2,28),1,28), date(2015,3,28))
        self.assertEqual(iara._acrescentar(date(2015,2,28),2,28), date(2015,4,28))

    def test_complexo(self):
        iara = Inicio(date(2016,1,31))
        prazos = iara.prazos(12,1)
        self.assertEqual(prazos[0], (date(2016,1,31),date(2016,3,1)))
        self.assertEqual(prazos[1], (date(2016,3,1),date(2016,3,31)))
        self.assertEqual(prazos[2], (date(2016,3,31),date(2016,5,1)))
        self.assertEqual(prazos[3], (date(2016,5,1),date(2016,5,31)))
        self.assertEqual(prazos[4], (date(2016,5,31),date(2016,7,1)))
        self.assertEqual(prazos[5], (date(2016,7,1),date(2016,7,31)))
        self.assertEqual(prazos[6], (date(2016,7,31),date(2016,8,31)))
        self.assertEqual(prazos[7], (date(2016,8,31),date(2016,10,1)))
        self.assertEqual(prazos[8], (date(2016,10,1),date(2016,10,31)))
        self.assertEqual(prazos[9], (date(2016,10,31),date(2016,12,1)))
        self.assertEqual(prazos[10], (date(2016,12,1),date(2016,12,31)))
        self.assertEqual(prazos[11], (date(2016,12,31),date(2017,1,31)))

    def test_complexo_intervalo_longo(self):
        iara = Inicio(date(2016,1,31))
        prazos = iara.prazos(6,5)
        self.assertEqual(prazos[0], (date(2016,1,31),date(2016,7,1)))
        self.assertEqual(prazos[1], (date(2016,7,1),date(2016,12,1)))
        self.assertEqual(prazos[2], (date(2016,12,1),date(2017,5,1)))
        self.assertEqual(prazos[3], (date(2017,5,1),date(2017,10,1)))
        self.assertEqual(prazos[4], (date(2017,10,1),date(2018,3,1)))
        self.assertEqual(prazos[5], (date(2018,3,1),date(2018,7,31)))

    def test_complexo_nao_coincidir(self):
        iara = Inicio(date(2016,1,31))
        prazos = iara.prazos(4,1,False)
        self.assertEqual(prazos[0], (date(2016,1,31),date(2016,2,29)))
        self.assertEqual(prazos[1], (date(2016,3,1),date(2016,3,30)))
        self.assertEqual(prazos[2], (date(2016,3,31),date(2016,4,30)))
        self.assertEqual(prazos[3], (date(2016,5,1),date(2016,5,30)))


if __name__ == '__main__':
    unittest.main()
