## Exemplos de uso

### Um prazo simples

A biblioteca calcula os prazos com base na data base, em intervalo de meses.
```python
>>> from datetime import date
>>> from prazos import Inicio
>>> inicio = Inicio(data_base=date(2020,1,1))
>>> inicio.prazo(intervalo=1)
datetime.date(2020,2,1)
>>>
>>> inicio.prazo(intervalo=3)
datetime.date(2020,4,1)
```
Por padrão os dias das data irão coincidir, caso deseje o contrário, passe False
como argumento para o parâmetro coincidir.
```python
>>> inicio.prazo(intervalo=1, coincidir=False)
datetime.date(2020,1,31)
>>>
>>> inicio.prazo(intervalo=3, coincidir=False)
datetime.date(2020,3,31)
```
### Prazos diversos

O método prazos permite obter diversos períodos a partir da data base.  
Passe como argumentos o intervalo entre os prazos e a quantidade pretendida.
```python
>>> from datetime import date
>>> from prazos import Inicio
>>> inicio = Inicio(data_base=date(2020,1,1))
>>> inicio.prazos(quantidade=2, intervalo=1)
((datetime.date(2020,1,1),datetime.date(2020,2,1)),(datetime.date(2020,2,1),datetime.date(2020,3,1)))
>>>
>>> inicio.prazos(quantidade=3, intervalo=1)
((datetime.date(2020,1,1),datetime.date(2020,2,1)),(datetime.date(2020,2,1),datetime.date(2020,3,1)),(datetime.date(2020,3,1),datetime.date(2020,4,1)))
```
Caso deseje que os dias das data não coincidam, passe True como argumento de
coincidir.
```python
>>> inicio.prazos(quantidade=3, intervalo=1, coincidir=False)
((datetime.date(2020,1,1),datetime.date(2020,1,31)),(datetime.date(2020,2,1),datetime.date(2020,2,29)),(datetime.date(2020,3,1),datetime.date(2020,3,31)))
```
## O diferencial
O algoritmo da biblioteca resolve o problema causado pela diferença na
quantidade de dias nos meses. Mas para isso, caso o mês não tenha o dia da data
base, ele irá considerar a primeira data posterior caso deseje coincidir os
dias.

Qual será o vencimento do prazo de um mês a partir de 31/01/2016?
29/02/2016 ou 01/03/2016?
Resposta: A seu gosto.
```python
>>> from datetime import date
>>> from prazos import Inicio
>>> inicio = Inicio(data_base=date(2016,1,31))
>>> inicio.prazo(intervalo=1)
datetime.date(2016,3,1)
>>>
>>> inicio.prazo(intervalo=1, coincidir=False)
datetime.date(2016,2,29)
```
E isso vale para prazos mais longos:
```python
>>> inicio.prazos(quantidade=3, intervalo=1)
((datetime.date(2016,1,31),datetime.date(2016,3,1)),(datetime.date(2016,3,1),datetime.date(2016,3,31)),(datetime.date(2016,3,31),datetime.date(2016,5,1)))
>>>
>>> inicio.prazos(quantidade=3, intervalo=1, coincidir=False)
((datetime.date(2016,1,31),datetime.date(2016,2,29)),(datetime.date(2016,3,1),datetime.date(2016,3,30)),(datetime.date(2016,3,31),datetime.date(2016,4,30)))
```
